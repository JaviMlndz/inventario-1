﻿using Desarrollo.Inventario.ModelosBASE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desarrollo.Inventario.Servicios
{
    public class CategoriaService
    {
        DataCategoriaDataContext _context = new DataCategoriaDataContext();


        public List<Categoria> ListadoCategorias(int? codigo,string nombre)
        {
            int cod = codigo == null ? 0 : Convert.ToInt32(codigo);
            string nombrecat = string.IsNullOrEmpty(nombre) ? "" : nombre;


            List<Categoria> listadocategorias = new List<Categoria>();
            var Listado = _context.SP_LISTAR_CATEGORIA(cod,nombrecat);

            foreach(var categoria in Listado)
            {
                Categoria Objcategoria = new Categoria()
                {
                    categoria1=categoria.categoria,
                    codigo_categoria=categoria.codigo_categoria,
                    detalles=categoria.detalles

                };

                listadocategorias.Add(Objcategoria);

            }

            return listadocategorias;


        }

        public bool GuardarCategoria(string nombre, string detalle)
        {
            int valor;
            bool resultado = false;
            int? reference=null;

            int guardar = _context.SP_AGREGAR_CATEGORIA(nombre, detalle, ref reference);

            if(guardar != 0 && guardar > 0)
            {
                resultado = true;
            }
            return resultado;
        }

        public int EliminarCategoria(int idcategoria)
        {
            int resultado = 0;
            
            try
            {
                _context.SP_ELIMINAR_CATEGORIA(idcategoria);
                resultado = 1;

            }
            catch (Exception)
            {

                resultado = 2;

            }

            return resultado;

        }

        public Categoria GetCategoria(int codigo) {

          
            var cat = _context.SP_GETCATEGORIA_C(codigo).First();

            Categoria categoria = new Categoria() {

                 categoria1=cat.categoria,
                codigo_categoria=cat.codigo_categoria,
                   detalles=cat.detalles
            };

            return categoria;

        }

        public bool ActualizarCategoria(int codigo, string nombrecat,string detallecat)
        {
            
            int valor = _context.SP_ACTUALIZARCATEGORIA(codigo, nombrecat, detallecat);

            return valor == 1 ? true : false;
            
        }
    }
}