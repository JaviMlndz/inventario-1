﻿using Desarrollo.Inventario.ModelosBASE;
using Desarrollo.Inventario.Models.CategoriasModel;
using Desarrollo.Inventario.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Westwind.Web.Mvc;

namespace Desarrollo.Inventario.Controllers
{
    public class CategoriaController : Controller
    {
        CategoriaService _CategoriaService = new CategoriaService();


        [Authorize]
        public ActionResult Listado(CategoriaListaVM listadocat)
        {
            
            List<Categoria> Categorias = _CategoriaService.ListadoCategorias(listadocat.codigo,listadocat.nombre);

            CategoriaListaVM objcat = new CategoriaListaVM()
            {
                codigo=listadocat.codigo,
                listadoCategorias= Categorias,
                 nombre=listadocat.nombre
                
               
            };
          

            return View(objcat);
        }

        [Authorize]
        public JsonResult GuardarCategoria(string nombre,string detalles)
        {
            bool exito = _CategoriaService.GuardarCategoria(nombre, detalles);

            List<Categoria> Categorias = _CategoriaService.ListadoCategorias(null,"");

            CategoriaListaVM objcat = new CategoriaListaVM()
            {
                listadoCategorias = Categorias,
               
            };
           
            string postsHtml = ViewRenderer.RenderPartialView("~/Views/Categoria/_listadoCategorias.cshtml", objcat);

            return Json(new { exito = exito, postsHtml = postsHtml }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult EliminarCategoria(int idcategoria)
        {
            int resultado = _CategoriaService.EliminarCategoria(idcategoria);
            
            return Json(new { exito = resultado }, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult GetCategoria(int codigo)
        {

            Categoria cataActual = _CategoriaService.GetCategoria(codigo);


            return Json(new { codigo = cataActual.codigo_categoria, cat = cataActual.categoria1, detalle = cataActual.detalles });

        }




        [Authorize]
        public JsonResult Editarcategoria(int codigo,string nombre, string detalles)
        {
            bool exito = _CategoriaService.ActualizarCategoria(codigo,nombre, detalles);

            List<Categoria> Categorias = _CategoriaService.ListadoCategorias(null, "");

            CategoriaListaVM objcat = new CategoriaListaVM()
            {
                listadoCategorias = Categorias,

            };

            string postsHtml = ViewRenderer.RenderPartialView("~/Views/Categoria/_listadoCategorias.cshtml", objcat);

            return Json(new { exito = true, postsHtml = postsHtml }, JsonRequestBehavior.AllowGet);
        }

    }
}