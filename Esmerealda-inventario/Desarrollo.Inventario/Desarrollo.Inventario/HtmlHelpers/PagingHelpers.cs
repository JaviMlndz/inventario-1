﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using Desarrollo.Inventario.WebAdmin.Models;

namespace Desarrollo.Inventario.WebAdmin.HtmlHelpers
{
    public static class PagingHelpers
    {

        public static MvcHtmlString PageLinksNB(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();

            int totalS = pagingInfo.CurrentPage * pagingInfo.ItemsPerPage;

            if (totalS > pagingInfo.TotalItems)
                totalS = pagingInfo.TotalItems;
            //pagina anterior
            if (pagingInfo.CurrentPage > 1)
            {
                TagBuilder tag1 = new TagBuilder("a");
                tag1.MergeAttribute("href", pageUrl(pagingInfo.CurrentPage - 1));
                tag1.InnerHtml = "Anterior";
                tag1.AddCssClass("botonNav");
                tag1.AddCssClass("btn btn-white");
                result.Append(tag1.ToString());

            }


            int totalP = 10;
            int ipage = 1;


            if (totalP > pagingInfo.TotalPages)
                totalP = pagingInfo.TotalPages;

            if (pagingInfo.CurrentPage > 6)
            {
                totalP = pagingInfo.CurrentPage + 4;
                ipage = pagingInfo.CurrentPage - 5;

                if (totalP > pagingInfo.TotalPages)
                {
                    totalP = pagingInfo.TotalPages;
                    ipage = pagingInfo.TotalPages - 9;


                }
            }

            for (int i = ipage; i <= totalP; i++)
            {

                TagBuilder tag = new TagBuilder("a");
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                {
                    tag.AddCssClass("active");
                    
                }
                   
                else
                    tag.MergeAttribute("href", pageUrl(i));

                tag.AddCssClass("btn btn-white");
                result.Append(tag.ToString());

            }

            //pagina siguiente
            if (pagingInfo.CurrentPage < pagingInfo.TotalPages)
            {
                TagBuilder tag1 = new TagBuilder("a");
                tag1.MergeAttribute("href", pageUrl(pagingInfo.CurrentPage + 1));
                tag1.InnerHtml = "Siguiente";
                tag1.AddCssClass("botonNav");
                tag1.AddCssClass("btn btn-white");
                result.Append(tag1.ToString());

            }


            return MvcHtmlString.Create(result.ToString());
        }


        public static MvcHtmlString fechaStr(this HtmlHelper html, DateTime fecha)
        {
            string result = "";
            if (fecha.DayOfWeek == DayOfWeek.Sunday)
                result = "Domingo";
            else if (fecha.DayOfWeek == DayOfWeek.Monday)
            { result = "Lunes"; }
            else if (fecha.DayOfWeek == DayOfWeek.Tuesday)
                result = "Martes";
            else if (fecha.DayOfWeek == DayOfWeek.Thursday)
                result = "Miercoles";
            else if (fecha.DayOfWeek == DayOfWeek.Wednesday)
                result = "Jueves";
            else if (fecha.DayOfWeek == DayOfWeek.Friday)
                result = "Viernes";
            else if (fecha.DayOfWeek == DayOfWeek.Saturday)
                result = "Sabado";

            result = result + ", " + fecha.Day.ToString() + " de ";
            if (fecha.Month == 1)
                result = result + " Enero de ";
            else if (fecha.Month == 2)
                result = result + " Febrero de ";
            else if (fecha.Month == 3)
                result = result + " Marzo de ";
            else if (fecha.Month == 4)
                result = result + " Abril de ";
            else if (fecha.Month == 5)
                result = result + " Mayo de ";
            else if (fecha.Month == 6)
                result = result + " Junio de ";
            else if (fecha.Month == 7)
                result = result + " Julio de ";
            else if (fecha.Month == 8)
                result = result + " Agosto de ";
            else if (fecha.Month == 9)
                result = result + " Septiembre de ";
            else if (fecha.Month == 10)
                result = result + " Octubre de ";
            else if (fecha.Month == 11)
                result = result + " Noviembre de ";
            else if (fecha.Month == 12)
                result = result + " Diciembre de ";
            result = result + fecha.Year.ToString();

            return MvcHtmlString.Create(result);
        }
    }
}