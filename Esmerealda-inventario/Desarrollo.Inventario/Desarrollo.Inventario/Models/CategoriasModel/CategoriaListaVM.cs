﻿using Desarrollo.Inventario.ModelosBASE;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Desarrollo.Inventario.Models.CategoriasModel
{
    public class CategoriaListaVM
    {

       public List<Categoria>listadoCategorias { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "El Codigo de*be tener sólo números.")]
       
        public  int? codigo { get; set; }
       public string nombre { get; set; }
    }
}