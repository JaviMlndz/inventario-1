﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Desarrollo.Inventario.WebAdmin.Models
{
    public class LoginVM
    {

        [Required(ErrorMessage = "El usuario es requerido")]
        public string usuario { get; set; }
        [Required(ErrorMessage = "La clave es requerida")]
        public string clave { get; set; }
        
        
    }
}