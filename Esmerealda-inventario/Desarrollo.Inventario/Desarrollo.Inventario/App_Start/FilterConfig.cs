﻿using System.Web;
using System.Web.Mvc;

namespace Desarrollo.Inventario.WebAdmin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
